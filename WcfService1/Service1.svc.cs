﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        //membuat variable koneksi ke sql
        string str = @"Data Source=.\SQLEXPRESS;Initial Catalog=SignalRDB;User ID=sa;Password=Admin123456kr";



        public string INSERTDATA(Stream param)
        {
            try
            {
                StreamReader reader = new StreamReader(param);
                string JSONdata = reader.ReadToEnd();
                JavaScriptSerializer jss = new JavaScriptSerializer();
                List<Ent_WSDATA> listWSDATA = jss.Deserialize<List<Ent_WSDATA>>(JSONdata);


                DataTable dtWSDATA = SQLFunct.ToDataTable<Ent_WSDATA>(listWSDATA);

                string connString = SQLFunct.GetConnString();
                SqlConnection con = new SqlConnection(connString);

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                SqlCommand command = new SqlCommand("SP_INSERT_WSDATA", con);
                command.Parameters.Add(new SqlParameter("@WsDataInsert", SqlDbType.Structured));
                command.Parameters["@WsDataInsert"].Value = dtWSDATA;
                command.Parameters["@WsDataInsert"].TypeName = "WSDATAype";

                string result = SQLFunct.ExecuteNonQuery(command);

                return "1";

            }
            catch (Exception x)
            {
                return x.Message;
            } 
        }
    }
}

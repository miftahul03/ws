﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WcfService1
{
    public class clsModul
    {
        public static string sqlMessage;

        public static string GetAppSetting(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings[key];
            }
            catch (Exception x)
            {
                return "";
            }
        }
        public static string GetConnString()
        {
            string connString = "";
            connString = GetAppSetting("ConString");

            return connString;
        }
        public static string ExecuteNonQuery(SqlCommand command)
        {
            string connString = GetConnString();
            string result = "";
            SqlConnection con = new SqlConnection(connString);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlTransaction transaction = con.BeginTransaction();
            try
            {
                command.Transaction = transaction;
                command.CommandTimeout = 600000;
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Connection = con;
                command.ExecuteNonQuery();

                transaction.Commit();
                transaction.Dispose();
                con.Close();
                con.Dispose();
                sqlMessage = "1";
                return sqlMessage;
            }
            catch (Exception x)
            {
                if (x.Message.Contains("Violation of PRIMARY KEY"))
                {
                    sqlMessage = x.Message;
                }
                else
                {
                    sqlMessage = "0";
                }
                transaction.Rollback();
                transaction.Dispose();
                con.Close();
                con.Dispose();
                return sqlMessage;
            }
            finally
            {
                con.Close();
            }
        }
    }

    
}